from django.db import models
from django.utils.timezone import now
# Create your models here.

class User(models.Model):
    name=models.CharField(max_length=100)
    email=models.CharField(max_length=50)
    contact=models.CharField(max_length=50)
    credit=models.FloatField(default=500)
    gender=models.CharField(max_length=40)

class Transaction(models.Model):
    received=models.CharField(max_length=100)
    sent=models.CharField(max_length=100)
    credit=models.FloatField()
    date=models.DateField(default=now)
